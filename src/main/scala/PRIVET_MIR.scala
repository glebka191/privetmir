import org.apache.spark.sql.{SaveMode, SparkSession}
import org.apache.spark.sql.functions._
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.io._

import scala.io.Source


object PRIVET_MIR {
  //  System.setProperty("HADOOP_USER_NAME", "hive")

  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder()
      .appName("TSC_PRIVET_MIR")
      .config("hive.metastore.uris", "thrift://dn01:9083")
      .master("yarn")
      .enableHiveSupport()
      .getOrCreate()


    val efr = "ods_efr" //args(0).toLowerCase
    val szp = "ods_szp" //args(1).toLowerCase
    val schemeDest = "prod_dm_mir"

    spark.sql(s"""create database if not exists ${schemeDest} location 'hdfs://dn01:8020/data/adwh/${schemeDest}'""")
    spark.sql(s"use $schemeDest")

    def currentDateTime(): String = {
      DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss:SSS").format(LocalDateTime.now)
    }

    def logToLocalFile(typeMsg: Int, msg: String, sb: StringBuilder): Unit =
      typeMsg match {
        case 0 => {
          sb.append(currentDateTime() + ";HDP PKB showcases forming job;Info;" + msg + "\n")
        }
        case 1 => {
          sb.append(currentDateTime() + ";HDP PKB showcases forming job;Warning;" + msg + "\n")
        }
        case 2 => {
          sb.append(currentDateTime() + ";HDP PKB showcases forming job;Error;" + msg + "\n")
        }
        case 3 =>
          sb.append("[DEBUG] schema:" + msg + "\n")
      }

    spark.sparkContext.setLogLevel("WARN")
    val nThreads = spark.sparkContext.defaultParallelism
    println(s"\n\n\nPrivet MIR\n\n\nStarting Spark with $nThreads threads.\n")

    val path = spark.sql(s"describe database ${schemeDest}").
      filter(col("database_description_item") === "Location").
      select("database_description_value").first().get(0)
    val logFileName = "/Privet_MIR.log"
    val tmp = "/tmp"

    val conf = new Configuration()
    val fs = FileSystem.get(conf)

    if (!fs.exists(new Path(s"$path$logFileName"))) {
      fs.create(new Path(s"$path$logFileName"))
    }

    val sb = new StringBuilder()

    //    val locFile = new File(s"$tmp$logFileName")

    //    val pw = new PrintWriter(locFile)

    logToLocalFile(0, "Privet MIR showcase forming is started", sb)

    try {
      logToLocalFile(0, "Previous data sampling", sb)

      if (spark.catalog.tableExists(s"${schemeDest}", "dm_card_mir") && !spark.catalog.tableExists(s"${schemeDest}", "tmp_dm_card_mir")) {
        spark.sql(s"""alter table ${schemeDest}.dm_card_mir rename to ${schemeDest}.tmp_dm_card_mir""")
      } else if (spark.catalog.tableExists(s"${schemeDest}", "dm_card_mir") && spark.catalog.tableExists(s"${schemeDest}", "tmp_dm_card_mir")) {
        spark.sql(s"""drop table ${schemeDest}.dm_card_mir""")
      }

      logToLocalFile(0, "Previous data sampling is finished", sb)

      logToLocalFile(0, "Implementation common operations", sb)

      spark.table(s"""${efr}.sblfr_soh_s_contact""")
        .filter(col("odsis_active_flg")
          .like("1"))
        .filter(col("odsdeleted_flg")
          .like("0"))
        .filter(current_timestamp.between("odseffective_from_dt", "odseffective_to_dt"))
        .select("last_name",
          "fst_name",
          "mid_name",
          "sex_mf",
          "birth_dt",
          "row_id",
          "pr_email_addr_id",
          "pr_alt_ph_num_id")
        .withColumn("sex", when(col("sex_mf")
          .like("Мужской"), "M")
          .when(col("sex_mf")
            .like("Женский"), "Ж")
          .otherwise(col("sex_mf")))
        .drop("sex_mf")
        .createOrReplaceTempView("sblfr_soh_s_contact_tmp_view")

      spark.table(s"""${efr}.sblfr_soh_s_per_comm_addr""")
        .filter(col("odsis_active_flg").like("1"))
        .filter(col("odsdeleted_flg").like("0"))
        .filter(current_timestamp.between("odseffective_from_dt", "odseffective_to_dt"))
        .select("addr", "row_id")
        .createOrReplaceTempView("sblfr_soh_s_per_comm_addr_tmp_view")

      logToLocalFile(0, "Common operations was implemenet", sb)

      logToLocalFile(0, "Data sampling for Asset", sb)

      var df1 = spark.table(s"""${efr}.h2_s_asset_x""")
        .filter(col("x_mir_lty_pgm_flg").like("Y"))
        .filter(col("odsis_active_flg").like("1"))
        .filter(col("odsdeleted_flg").like("0"))
        .filter(current_timestamp.between("odseffective_from_dt", "odseffective_to_dt"))
        .select("row_id")

      var tmpDf = spark.table(s"""${efr}.h2_s_asset""")
        .filter(col("odsis_active_flg").like("1"))
        .filter(col("odsdeleted_flg").like("0"))
        .filter(current_timestamp.between("odseffective_from_dt", "odseffective_to_dt")).
        select("asset_num",
          "row_id",
          "pr_con_id")

      df1 = df1.join(tmpDf, "row_id")
        .select("asset_num", "pr_con_id")

      tmpDf = spark.table("""sblfr_soh_s_contact_tmp_view""")

      df1 = df1.join(tmpDf, df1("pr_con_id") === tmpDf("row_id"), "inner")
        .select(
          "asset_num",
          "last_name",
          "fst_name",
          "mid_name",
          "sex",
          "birth_dt",
          "pr_email_addr_id",
          "pr_alt_ph_num_id")

      tmpDf = spark.table("""sblfr_soh_s_per_comm_addr_tmp_view""")

      df1 = df1.join(tmpDf, df1("pr_email_addr_id") === tmpDf("row_id"), "left").
        drop("row_id",
          "pr_email_addr_id").
        withColumnRenamed("addr", "email")

      df1 = df1.join(tmpDf, df1("pr_alt_ph_num_id") === tmpDf("row_id"), "left").
        drop("row_id",
          "pr_alt_ph_num_id").
        withColumnRenamed("addr", "mobile_phone").
        select("last_name",
          "fst_name",
          "mid_name",
          "sex",
          "birth_dt",
          "email",
          "mobile_phone",
          "asset_num").
        withColumnRenamed("birth_dt", "birthday").
        withColumnRenamed("asset_num", "card_number")

      logToLocalFile(0, "Data sampling is finished", sb)

      logToLocalFile(0, "Data sampling for OPTY", sb)

      var df2 = spark.table(s"""${efr}.h2_s_opty_x""")
        .filter(col("x_mir_lty_pgm_flg").like("Y"))
        .filter(col("odsis_active_flg").like("1"))
        .filter(col("odsdeleted_flg").like("0"))
        .filter(current_timestamp.between("odseffective_from_dt", "odseffective_to_dt"))
        .select("row_id",
          "card_num")

      tmpDf = spark.table(s"""${efr}.sblfr_soh_s_opty""").
        filter(col("odsis_active_flg").like("1")).
        filter(col("odsdeleted_flg").like("0")).
        filter(current_timestamp.between("odseffective_from_dt", "odseffective_to_dt")).
        select("row_id",
          "pr_con_id")

      df2 = df2.join(tmpDf, "row_id").
        drop("row_id")

      tmpDf = spark.table("""sblfr_soh_s_contact_tmp_view""")

      df2 = df2.join(tmpDf, df2("pr_con_id") === tmpDf("row_id"), "inner").
        drop("row_id",
          "pr_con_id")

      tmpDf = spark.table("""sblfr_soh_s_per_comm_addr_tmp_view""")

      df2 = df2.join(tmpDf, df2("pr_email_addr_id") === tmpDf("row_id"), "left").
        drop("row_id",
          "pr_email_addr_id").
        withColumnRenamed("addr", "email")

      df2 = df2.join(tmpDf, df2("pr_alt_ph_num_id") === tmpDf("row_id"), "left").
        drop("row_id",
          "pr_atl_ph_num_id").
        withColumnRenamed("addr", "mobile_phone").
        select("last_name",
          "fst_name",
          "mid_name",
          "sex",
          "birth_dt",
          "email",
          "mobile_phone",
          "card_num").
        withColumnRenamed("birth_dt", "birthday").
        withColumnRenamed("card_num", "card_number")

      logToLocalFile(0, "Data sampling is finished", sb)

      logToLocalFile(0, "Data sampling for SRV_REQ", sb)

      var df3 = spark.table(s"""${efr}.h2_s_srv_req_x""").
        filter(col("x_mir_lty_pgm_flg").like("Y")).
        filter(col("odsis_active_flg").like("1")).
        filter(col("odsdeleted_flg").like("0")).
        filter(current_timestamp.between("odseffective_from_dt", "odseffective_to_dt")).
        select("row_id")

      tmpDf = spark.table(s"""${efr}.sblfr_soh_s_srv_req""").
        filter(col("odsis_active_flg").like("1")).
        filter(col("odsdeleted_flg").like("0")).
        filter(current_timestamp.between("odseffective_from_dt", "odseffective_to_dt")).
        select("x_card_num",
          "row_id",
          "cst_con_id")

      df3 = df3.join(tmpDf, "row_id").
        drop("row_id")

      tmpDf = spark.table("""sblfr_soh_s_contact_tmp_view""")

      df3 = df3.join(tmpDf, df3("cst_con_id") === tmpDf("row_id"), "inner").
        drop("row_id",
          "cst_con_id")

      tmpDf = spark.table("""sblfr_soh_s_per_comm_addr_tmp_view""")

      df3 = df3.join(tmpDf, df3("pr_email_addr_id") === tmpDf("row_id"), "left").
        drop("pr_email_addr_id",
          "row_id").
        withColumnRenamed("addr", "email")

      df3 = df3.join(tmpDf, df3("pr_alt_ph_num_id") === tmpDf("row_id"), "left").
        drop("pr_alt_ph_num_id",
          "row_id").
        withColumnRenamed("addr", "mobile_phone").
        select("last_name",
          "fst_name",
          "mid_name",
          "sex",
          "birth_dt",
          "email",
          "mobile_phone",
          "x_card_num").
        withColumnRenamed("birth_dt", "birthday").
        withColumnRenamed("x_card_num", "card_number")

      logToLocalFile(0, "Data sampling is finished", sb)

      logToLocalFile(0, "Data sampling for SZP cards", sb)

      val df4 = spark.table(s"""${szp}.h2_dm_card_mir_agr""")
        .filter(col("agreement_flag").like("Y"))
        .filter(col("odsis_active_flg").like("1"))
        .filter(col("odsdeleted_flg").like("0"))
        .filter(current_timestamp.between("odseffective_from_dt", "odseffective_to_dt")).
        select(
          col("last_name"),
          col("first_name").as("fst_name"),
          col("middle_name").as("mid_name"),
          col("sex"),
          col("birthday"),
          col("email"),
          col("mobile_phone"),
          col("card_number")
        )

      logToLocalFile(0, "Data sampling is finished", sb)

      //      logToLocalFile(0, "Privet MIR showcase forming", sb)


      df1 = df1.union(df2).union(df3).union(df4)

      df1.repartition(1)
        .write.mode(SaveMode.Append)
        .parquet(s"${schemeDest}.dm_card_mir") //Пока ориентировочно так

      println("!")

      val insertCounts = df1.count

      println("!")

      println(s"записей сделано: $insertCounts")

      logToLocalFile(0, s"Privet MIR showcase forming is finished;$insertCounts", sb)
    } catch {
      case ex: Throwable => {
        logToLocalFile(2, ex.toString, sb)

        logToLocalFile(0, "Loading previous data into showcase", sb)

        spark.sql(s"""drop table if exists ${schemeDest}.dm_card_mir""")

        if (spark.catalog.tableExists(s"${schemeDest}", "tmp_dm_card_mir")) {
          spark.sql(s"""alter table ${schemeDest}.tmp_dm_card_mir rename to ${schemeDest}.dm_card_mir""")
        }

        logToLocalFile(0, "Loading previous data is finished", sb)
      }
    }

    //    pw.close()

    val fileOutputStream = fs.append(new Path(s"$path$logFileName"))
    //    val bufferedSource = Source.fromFile(s"$tmp$logFileName")
    //
    //    for (line <- bufferedSource.getLines) {
    //      fileOutputStream.write(s"$line\n".getBytes("UTF-8"))
    //    }
    //    bufferedSource.close()
    fileOutputStream.write(sb.toString().getBytes("UTF-8"))
    fileOutputStream.flush()
    fileOutputStream.close()

    //    fs.delete(new Path(s"$tmp$logFileName"), true)

    spark.close
  }
}